# Alves

bl_info = {
    "name": "Data Renamer",
    "author": "Damien Picard",
    "version": (0, 0, 1),
    "blender": (2, 80, 0),
    "location": "3D View > Tool",
    "description": "Rename objects",
    "doc_url": "",
    "category": "Layout",
}

# Copyright (C) 2021  Damien Picard dam.pic AT free.fr
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import bpy


class OBJECT_OT_Rename(bpy.types.Operator):
    """Rename data in the scene"""
    # bl_idname doit avoir deux parties séparées par un "." point
    # C'est l'identifiant de l'opérateur
    bl_idname = "object.rename_objects"
    bl_label = "Rename All Objects"  # nom apparaissant dans l'interface
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):
        rename_from = context.scene.rename_from
        rename_to = context.scene.rename_to

        data_type = context.scene.data_type
        # print(data_type)

        if data_type == "OBJECT":
            for obj in bpy.data.objects:
                obj.name = obj.name.replace(rename_from, rename_to)
                obj.data.name = obj.name

        if data_type == "DATA":
            for obj in bpy.data.objects:
                if obj.data is not None:
                    obj.data.name = obj.data.name.replace(rename_from, rename_to)

        if data_type == "MATERIAL":
            for mat in bpy.data.materials:
                mat.name = mat.name.replace(rename_from, rename_to)
                mat.name = mat.name
        return {'FINISHED'}


class OBJECT_PT_Rename(bpy.types.Panel):
    bl_label = "Rename Objects"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_category = "Tool"

    def draw(self, context):
        # Doc du layout :
        # https://docs.blender.org/api/current/bpy.types.UILayout.html

        layout = self.layout
        layout.use_property_split = True

        col = layout.column(align=True)
        col.prop(context.scene, "data_type")
        col.prop(context.scene, "rename_from")
        col.prop(context.scene, "rename_to")

        col = layout.column()
        col.operator("object.rename_objects")


def register():
    """register() est appelée quand on active un add-on.
    Ca dit à Blender quoi faire pour ajouter
    les opérateurs à la liste d'opérateurs,
    les panneaux aux interfaces,
    les propriétés aux datablocks,
    etc.
    """
    bpy.utils.register_class(OBJECT_OT_Rename)
    bpy.utils.register_class(OBJECT_PT_Rename)

    # TODO add icons
    type_items = [
        ("OBJECT", "Object", "",  "OBJECT_DATA", 1),
        ("DATA", "Data", "", "MESH_DATA", 2),
        ("MATERIAL", "Material", "", "MATERIAL_DATA", 3),
    ]
    bpy.types.Scene.data_type = bpy.props.EnumProperty(name='Data Type',
                                                       description='Type of data to rename',
                                                       items=type_items)
    bpy.types.Scene.rename_from = bpy.props.StringProperty(name='Rename from',
                                                           description='String to look for in collections and objects',
                                                           default='Truc')
    bpy.types.Scene.rename_to = bpy.props.StringProperty(name='Rename to',
                                                         description='What to replace the string with',
                                                         default='Machin')


def unregister():
    """unregister() est appelée quand on désactive un add-on.
    Ca fait l'inverse de register() : supprimer les fonctionnalités
    qu'on a ajoutées à Blender
    """
    del bpy.types.Scene.data_type
    del bpy.types.Scene.rename_from
    del bpy.types.Scene.rename_to
    bpy.utils.unregister_class(OBJECT_OT_Rename)
    bpy.utils.unregister_class(OBJECT_PT_Rename)


# Pour pouvoir enregistrer les classes, etc. depuis l'éditeur de texte,
# il faut appeler register() à la main
if __name__ == "__main__":
    register()
